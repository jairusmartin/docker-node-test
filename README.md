
Build Image:

    docker build . -t <your username>/node-web-app

Run App:

    docker run -p 8100:8100 -d jpm/nodewebapp

Visit Hello_World Page

    [http://localhost:8100](http://localhost:8100)
